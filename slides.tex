\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=Slow progress on app save/restore support,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[backend=biber]{biblatex}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

% Increase the space after the footnotes so that \footfullcite works
\addtobeamertemplate{footline}{\hfill\usebeamertemplate***{navigation symbols}%
    \hspace*{0.1cm}\par\vskip 20pt}{}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}


% Abstract here: https://events.gnome.org/event/101/contributions/453/
%
% Over the last year, some progress has slowly been made on adding support for
% saving and restoring app state across restarts. This requires changes in
% gnome-session, GTK and GLib, as well as in apps themselves.
%
% This talk will give an overview of where we’re at, the architecture being
% used, and what’s left to do. It will be a technical talk, and is aimed at
% toolkit developers and adventurous app authors.


\title{Slow progress on app save/restore support}

\author{Philip Withnall\\Endless\\\texttt{philip@tecnocode.co.uk}}
\date{July 28, 2023}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 20 minutes for talk, 5 minutes for questions.

Hello. Today I’m going to talk about where we’re at with app save/restore
support, the architecture being proposed for it, and what’s left to do. I should
probably stifle anyone’s hopes right now: this is not a big reveal talk in which
I show that it’s actually all complete and ready for app authors to use. It’s a
long way from that, but hopefully where we’re at now is interesting enough to
share.}

\note{This project is something I’ve been working on in periodic free hacking
time at Endless, for a couple of years now. It’s based on work done by Bastien
Nocera a few years ago, and incorporating ideas from Emmanuele Bassi. As far
back as 10 years ago, people were having discussions and ideas about how to
implement this, so there’s a long history of work this builds on. On the GTK
side of things, Matthias Clasen has done some experiments in the last couple of
years with widget tree serialisation/deserialisation, with the aim of helping
implement this feature.}


\begin{frame}{What is app save/restore support?}
\begin{itemize}
	\item{Reopening an app in the same state as it was when it was closed}
	\item{Closing by user action or by system policy}
	\item{gnome-session used to support a limited version of this in the olden days}
\end{itemize}
\end{frame}

\note{Firstly, what is app save/restore support?
It is what it says on the tin: restoring an app to the same state it was
in when it was closed, when it’s next opened. For example, with the same
windows showing, in the same position, with the same list items selected and
the same scroll position in views.}

\note{This could happen either when you log out and log in again, or when the
system decides to free up resources by killing a process and then transparently
restarting it when you switch focus to it again. Android does this, but we’re
a long way off supporting it.}

\note{gnome-session used to support a very limited version of app save/restore,
in that it
would save the list of apps you had open when logging out, and start those apps
again when you next logged in. The apps would start from fresh, though, with
none of their internal state saved. This was supported (I believe) until
GNOME 3. In particular, when \texttt{systemd -{}-user} support was added to
gnome-session, the save/restore support broke.}


\begin{frame}{Use cases}
\begin{itemize}
  \item{Restore apps to largely the same state when logging out and in again}
  \item{Restore apps to largely the same state if they are killed and later restarted by the session manager due to resource constaints}
  \item{Restore apps to largely the same state if they are updated and restarted due to an update (e.g. flatpaks)}
  \item{Allowing apps to choose \textit{not} to be restored (even if the data is available) if it would not be appropriate}
\end{itemize}
\end{frame}

\note{So I’ve been working on parts of app save/restore on and off for a couple
of years now, in various periods of hacking time provided by my employer,
Endless. The use cases I’ve had in mind are these (I won’t read them out).}


\begin{frame}{Use cases}
\begin{itemize}
	\item{\textbf{Not}: Allowing apps to checkpoint their state or provide an undo stack}
  \item{\textbf{Not}: Allowing apps to be restored if they are killed unexpectedly, crash, or power is lost}
\end{itemize}
\end{frame}

\note{There are also a few things which are explicitly \emph{not} use cases for
this feature. This is to prevent it ballooning into something which can track
arbitrary app state over time.}


\begin{frame}{Main components}
\begin{description}
	\item[App]{Decide what to save and restore}
	\item[Session manager]{Store the list of apps to restart, manage saving and restoring them
	  \begin{itemize}
    	\item{(While we’re there: might as well store the apps’ data too)}
    \end{itemize}}
	\item[UI toolkit]{Serialise/Deserialise the app’s UI}
	\item[Shell]{Decide when to kill/restart an app due to lack of resources}
\end{description}
\end{frame}

\begin{frame}{Proposed architecture}
\begin{figure}
	\includegraphics[width=0.55\columnwidth]{system.pdf}
	\caption{Proposed system architecture}
\end{figure}
\end{frame}

\note{Overall, it should actually be pretty simple to implement save/restore
support in a single app. It actually only takes changes in the app itself to
serialise and deserialise its state to a file on start and exit. The thing is,
in order to have the app correctly restarted at the start of the session, you
need help from the session manager. And it needs to have a standard interface
to talk to, so you need help from the toolkit. And it would be helpful for the
toolkit to provide a standard interface for serialising/deserialising a widget
hierarchy. And then in order to implement the use case for closing apps when the
system is low on memory, you need to know which app is focused, so the Shell
becomes involved. So before you know it, the project involves four different
components (or five if you count GTK and GLib separately).}


\begin{frame}{Proposed architecture}
\begin{itemize}
	\item{Extend the session manager protocol}
	\item{Apps get their restore data when they register with the session manager}
	\item{Save their data when the session ends (if they’re still running)}
	\item{Expose their data on \texttt{org.gtk.Application} as well, so it can be queried any time}
	\item{Data is an arbitrary \texttt{GVariant}}
\end{itemize}
\end{frame}

\note{For the moment, let’s ignore anything to do with the shell, as that’s not
something I’ve looked at yet. With that out of the way, the core of the
proposed architecture is some changes to the session management D-Bus API.
This is used for apps to communicate with gnome-session, so it knows what
processes are running in the user’s login session. It uses this to move them to
the right cgroup, to tell them when the session is going to end (and to allow
them to inhibit logout/shutdown), and now to get their save/restore data from
them.}


\begin{frame}{Proposed architecture: restoring}
\begin{figure}
	\includegraphics[width=\columnwidth]{register-client-restartable.png}
	\caption{Registering a restartable client on session start}
\end{figure}
\end{frame}

\note{This is the architecture I’ve ended up with for restoring app state, which
happens on startup. gnome-session will auto-start a list of apps saved from the
last session. When each of them registers with the session (historically via the
\texttt{RegisterClient} method), the session manager will now send their restart
data back to them. The app can then deserialise this to restore their state.}

\note{This fits in well because the session management API calls happen before
the app shows any UI, and happen already, so this approach adds no additional
D-Bus traffic. Restore data can be loaded from disk once by the session manager,
rather than N times by N apps.}

\note{The diagram shows a Bustle trace of the D-Bus traffic for the proposed
API. In it, the session manager auto-starts an app, the app registers with the
session manager and receives its restart data, and then its normal startup
process continues.}


\begin{frame}{Proposed architecture: saving}
\begin{figure}
	\includegraphics[width=\columnwidth]{end-session-response-restartable.png}
	\caption{Saving app state when ending the session}
\end{figure}
\end{frame}

\note{The architecture for saving app state is similar. It happens when the
session ends (when the user logs out, shuts down, restarts). The session manager
signals all apps querying whether they’re OK with the session ending. At this
point they have an opportunity to inhibit the session ending, for example
because they have unsaved documents. Once all documents are saved, the session
manager notifies all apps that the session is ending, and they respond with
\texttt{EndSessionResponseRestartable}, which acknowledges the session ending,
and contains their serialised restart data, or a boolean saying they explicitly
don’t want to be restarted on next session start.}

\note{The \texttt{EndSessionResponseRestartable} call is where this protocol
differs from before, as the restart data has been added to it. As with restoring
an app, this protocol change introduces no new D-Bus calls, and happens after
the app has been notified of session end --- so it has opportunity to prevent
the user from interacting with the UI further, and hence there are no races
when saving app state.}


\begin{frame}[fragile]
\frametitle{Application interface}
\begin{lstlisting}[language=C]
struct _GApplicationClass
{
  GVariant *(* build_restart_data)  (GApplication  *application,
                                     char         **out_tag);

  void      (* consume_restart_data)(GApplication  *application,
                                     const char    *tag,
                                     GVariant      *data);
};
\end{lstlisting}
\end{frame}

\note{The interface between application code and the toolkit is this pair of
new virtual methods on \texttt{GApplicationClass}. The intention is that they
could either be implemented directly by apps (if they want to do their own
serialisation) or chained up to GTK (to use its serialisation). In either case,
the app would probably have to explicitly opt in to restart support.}


\begin{frame}{What’s implemented?}
\begin{itemize}
	\item{Application interface is ready for review in GLib}
	\item{Session manager API changes ready for initial review}
	\item{GTK session management changes are the right shape}
	\item{GTK widget tree serialisation/deserialisation not done}
	\item{Shell integration not thought about}
\end{itemize}
\end{frame}

\note{What’s implemented is basically what I’ve run through so far. I have
enough changes locally to be able to run \texttt{gnome-session} and have it
start \texttt{gnome-calendar} with saved data, then query the calendar for data
at the end of the session and save that. I’m not brave enough to try and demo
that in this talk, so you’ll just have to take my word for it! It wouldn’t be
very exciting anyway, because the exciting visual part would come from the GTK
widget tree serialisation/deserialisation, and none of that is implemented yet.}


\begin{frame}{Current problem points}
\begin{itemize}
	\item{Little thought given to non-GNOME desktop, non-GTK apps}
	\item{Little thought given to XSMP apps}
	\item{Prototype for GTK serialisation is a few years old and I haven’t incorporated it}
	\item{API has to be stable, so it has to be right}
\end{itemize}
\end{frame}

\note{Of the things which have been written so far, there are a few areas which
I think could do with some more thought, which I haven’t done yet. Eventually,
this should work cross-desktop, cross-toolkit, and degrade nicely for old apps
which don’t support it (even the really old apps which speak XSMP rather than
the D-Bus gnome-session protocol we’ve used for the last decade plus). So
validating the API changes from these points of view would be useful.}


\begin{frame}{What’s next?}
\begin{itemize}
  \item{Help to validate the architecture}
	\item{Come to the GTK BoF session (\href{https://events.gnome.org/event/101/contributions/534/}{Saturday, 10:00})!}
	\item{Landing some initial bits}
	\item{gnome-session changes}
	\item{Future work on GTK and Shell}
\end{itemize}
\end{frame}

\note{What’s next? I need your help! If you’re interested in this and can
contribute to any of the necessary components, please come to the GTK BoF
session on Saturday. We’ll be discussing app save/restore as part of the session
there.}

\note{In terms of landing things, the GLib changes are ready for review and
hopefully to be landed this cycle. The gnome-session changes are almost ready for
review, but I expect there will be a lot of comments on them. Particularly, I’m
interested in high-level comments on the D-Bus API changes at the moment.
The GTK changes at the moment are quite minimal and tightly paired to the
gnome-session changes. Perhaps a good way to land the gnome-session changes
would be to gate them behind a configuration option to begin with, then they
could land earlier and get more testing.}

\note{There will need to be a lot of separate work done on
serialising/deserialising widget trees in GTK to make that convenience available
for apps. And I haven’t looked at the Shell changes at all, which is what’s
necessary for killing apps when system resources become scarce.}


\begin{frame}{Miscellany}
\begin{description}
	\item[Slide source]{\url{https://gitlab.gnome.org/pwithnall/guadec-app-save-restore-presentation-2023}}
	\item[GLib merge request]{\url{https://gitlab.gnome.org/GNOME/glib/-/merge_requests/683}}
	\item[gnome-session branch]{\url{https://gitlab.gnome.org/pwithnall/gnome-session/-/tree/session-restore}}
	\item[GTK branch]{\url{https://gitlab.gnome.org/pwithnall/gtk/-/tree/save-state}}
\end{description}


% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2023}}
\end{center}
\end{frame}

\end{document}
